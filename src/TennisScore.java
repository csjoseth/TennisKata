public class TennisScore {

    private String score;
    public int player1Points;
    public int player2Points;
    public int player1Games;
    public int player2Games;


    public TennisScore() {
        score = "Love-All";
        player1Points = 0;
        player2Points = 0;
        player1Games = 0;
        player2Games = 0;
    }

    public String getScore() {
        if (isGame()) {
            if (player1Points > player2Points)
                score = "Game player1";
            else
                score = "Game player2";
        } else {
            if (player1Points == player2Points){
                if  (player1Points < 3)
                    score = playerScoreToString(player1Points) + "-All";
                else
                    score = "Deuce";
            } else {
                if (isAdvantage()) {
                    if (player1Points > player2Points)
                        score = "Advantage player1";
                    else
                        score = "Advantage player2";
                } else
                    score = playerScoreToString(player1Points) + "-" + playerScoreToString(player2Points);
            }

        }


        return score;
    }

    private boolean isGame() {
        if (player1Points >= 4 || player2Points >= 4) {
            if (player1Points -2 >= player2Points || player2Points -2 >= player1Points) {
                return true;
            }
        }
        return false;
    }

    private boolean isAdvantage() {
        boolean advantage = false;
        if (player1Points >= 3 && player2Points >= 3) {
            if (player1Points + 1 == player2Points || player2Points +1 == player1Points)
                advantage = true;
        }
        return advantage;

    }

    public void addPointPlayer1() {
        if (isGame())
            resetScore();

        player1Points++;

        if (isGame())
            wonGame();
    }

    private void wonGame() {
        if (player1Points > player2Points) {
            player1Games++;
        }else {
            player2Games++;
        }
    }

    private void resetScore() {
        player1Points = 0;
        player2Points = 0;
    }

    public void addPointPlayer2() {
        if (isGame())
            resetScore();

        player2Points++;

        if (isGame())
            wonGame();
    }

    private String playerScoreToString(int playerScore) {
        switch (playerScore) {
            case 1: return "Fifteen";
            case 2: return "Thirty";
            case 3: return "Forty";
            default: return "Love";
        }
    }
}
