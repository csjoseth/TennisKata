import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class TennisScoreTests {

    private TennisScore tennisScore;

    @Before
    public void setUp() {
        tennisScore = new TennisScore();
    }

    @Test
    public void canGetScoreLoveAll() {
        assertEquals("Love-All", tennisScore.getScore());
    }

    @Test
    public void canAddPointPlayer1() {
        tennisScore.addPointPlayer1();
        assertEquals(1, tennisScore.player1Points);
    }

    @Test
    public void canAddPointPlayer2() {
        tennisScore.addPointPlayer2();
        assertEquals(1, tennisScore.player2Points);
    }

    @Test
    public void scoreShouldBeFifteenLove() {
        tennisScore.addPointPlayer1();
        assertEquals("Fifteen-Love", tennisScore.getScore());
    }

    @Test
    public void scoreShouldBeThirtyLove() {
        tennisScore.addPointPlayer1();
        tennisScore.addPointPlayer1();
        assertEquals("Thirty-Love", tennisScore.getScore());
    }

    @Test
    public void scoreShouldBeFortyLove() {
        for (int i = 0; i < 3; i++) {
            tennisScore.addPointPlayer1();
        }
        assertEquals("Forty-Love", tennisScore.getScore());
    }

    @Test
    public void scoreShouldBeDeuce() {
        for (int i = 0; i < 3; i++) {
            tennisScore.addPointPlayer1();
            tennisScore.addPointPlayer2();
        }
        assertEquals("Deuce", tennisScore.getScore());
    }

    @Test
    public void scoreShouldBeAdvantagePlayer1() {
        for (int i = 0; i < 3; i++) {
            tennisScore.addPointPlayer1();
            tennisScore.addPointPlayer2();
        }
        tennisScore.addPointPlayer1();
        assertEquals("Advantage player1", tennisScore.getScore());
    }

    @Test
    public void scoreShouldBeGamePlayer1() {
        for (int i = 0; i < 4; i++) {
            tennisScore.addPointPlayer1();
        }
        assertEquals("Game player1", tennisScore.getScore());
    }

    @Test
    public void shouldResetScoreAfterWonGameAndStartNew() {
        for (int i = 0; i < 5; i++) {
            tennisScore.addPointPlayer1();
        }
        assertEquals("Fifteen-Love", tennisScore.getScore());
    }

    @Test
    public void shouldIncrementPlayer1GameByOne() {
        for (int i = 0; i < 4; i++) {
            tennisScore.addPointPlayer1();
        }
        assertEquals(1, tennisScore.player1Games);
    }

    @Test
    public void shouldIncrementPlayer2GameByOne() {
        for (int i = 0; i < 4; i++) {
            tennisScore.addPointPlayer2();
        }
        assertEquals(1, tennisScore.player2Games);
    }



















}
